import { extname } from 'path'
import type { Node, NodeTag } from 'posthtml-parser'
import { parser } from 'posthtml-parser'
import { render } from 'posthtml-render'
import type { CacheStore, DemoAttr } from '../typing'
import { decodeData } from './parser-cache'
import { isObject } from './utils'
import type { Parser } from './index'

/**
 * @description: parse demo to code
 * @param src
 * @param md
 */
const parserSrc = async(src: string, md: Parser): Promise<CacheStore | undefined> => {
  const demoPath = md.getDemoPath(src)
  const relativePath = md.getBaseDemoPath(demoPath)
  if (md.cacheStore.has(relativePath)) {
    return md.cacheStore.get(relativePath)
  }
  else {
    console.error(`[vitepress-plugin-demo] ${src} not found`)
    return undefined
  }
}

const parserAttr = async(md: Parser, attrs?: Record<string, any>): Promise<DemoAttr> => {
  if (!attrs)
    return {}
  const raw = Reflect.has(attrs, 'raw')
  // console.log('attrs', attrs)
  const ext = extname(attrs.src)
  const code1 = await parserSrc(attrs.src, md)
  const code = code1?.code
  const docs: any[] = code1?.docs ?? []
  const title = docs.length > 0 ? docs[0].title : attrs?.title
  const desc = docs.length > 0 ? (docs[0].desc || attrs?.desc) : attrs?.desc
  const highlight = code1?.highlight
  let otherSrcArr: string|undefined
  // otherSrcArr预期是一个字符串数组，按就按照vue组件数组属性的命名风格解析
  if (attrs[':otherSrcArr'])
    otherSrcArr = attrs[':otherSrcArr']

  else if (attrs[':other-src-arr'])
    otherSrcArr = attrs[':other-src-arr']
  else otherSrcArr = undefined

  return {
    raw,
    title,
    desc,
    src: attrs.src,
    otherSrcArr,
    link: attrs.link,
    ext,
    code,
    highlight,
  }
}

const checkRaw = (demo: string, attrs: DemoAttr, md: Parser) => {
  const ext = attrs?.ext?.slice(1) ?? 'html'
  const code = attrs.code ?? ''
  if (code) {
    const codeInfo = md.renderCode(decodeData(code)!, ext, false)
    md.replaceCode(demo, codeInfo)
  }
}

const generateDemo = (demo: string, attrs: DemoAttr, node: NodeTag, nodes: Node[], md: Parser, replace = true) => {
  let otherSrcArr: string[]|undefined|string = []
  if (attrs.otherSrcArr) {
    otherSrcArr = JSON.parse(`${attrs.otherSrcArr.replace(/'/g, '"')}`) as string[]
    otherSrcArr = otherSrcArr.map((singleSrr) => {
      let tmpSrc = md.getDemoPath(singleSrr)
      tmpSrc = tmpSrc.startsWith('/') ? tmpSrc : `/${tmpSrc}`
      return tmpSrc
    })
    // 之所以又转换为字符串是因为下面的node.attrs的值只能是字符串，否则const html = render(node)这段代码不会渲染非字符串值的属性
    otherSrcArr = JSON.stringify(otherSrcArr).replace(/"/g, '\'')
  }
  else {
    otherSrcArr = undefined
  }
  // console.log('otherSrcArr', otherSrcArr)

  let src = md.getDemoPath(attrs.src)
  src = src.startsWith('/') ? src : `/${src}`
  const liveCodeOption: Record<string, any> = {}
  if (md.options?.codeSandBox?.url) {
    liveCodeOption.codeSandBox = {
      ':codeSandBox': md.options.codeSandBox.url,
    }
  }
  if (md.options?.stackblitz?.url) {
    liveCodeOption.stackblitz = {
      ':stackblitz': md.options.stackblitz.url,
    }
  }
  const title = attrs.title
  const desc = attrs.desc
  node.attrs = {
    src,
    // vue的非字符串属性需要使用`:属性名`的格式
    ':otherSrcArr': otherSrcArr,
    title,
    desc,
    'link': attrs.link,
    ...liveCodeOption,
  } as Record<string, any>
  if (replace) {
    const html = render(node)
    md.replaceCode(demo, html)
  }
}

const chunkMultiDemo = async(demo: string, nodes: Node[], md: Parser) => {
  const newNodes: Node[] = []
  for (const node of nodes) {
    if (isObject(node) && node.tag === md.wrapper) {
      const attrs = await parserAttr(md, node.attrs)
      if (attrs.raw || !md.checkSupportExt(attrs.ext)) {
        const ext = attrs?.ext?.slice(1) ?? 'html'
        const code = attrs.code ?? ''
        const codeInfo = md.renderCode(decodeData(code)!, ext, false)
        const parserNode = parser(codeInfo)
        newNodes.push(...parserNode)
      }
      else {
        generateDemo(demo, attrs, node, nodes, md, false)
        newNodes.push(node)
      }
    }
    else {
      newNodes.push(node)
    }
  }
  const html = render(newNodes)
  md.replaceCode(demo, html)
}

export const parserDemo = async(demos: string[], md: Parser) => {
  const deduplicateDemos = [...new Set(demos || [])]
  for (const demo of deduplicateDemos) {
    const nodes: Node[] = parser(demo)
    /**
     * 处理单个和多个 demo
     */
    if (nodes.length === 1) {
      const node: NodeTag = nodes[0] as NodeTag
      // 从markdown源代码中解析attrs
      const attrs = await parserAttr(md, node.attrs)
      if (attrs.raw || !md.checkSupportExt(attrs.ext)) {
        // 当前是源码模式
        checkRaw(demo, attrs, md)
      }
      else {
      // 当前不是raw模式
        // console.log('comp attrs', attrs)
        generateDemo(demo, attrs, node, nodes, md)
      }
    }
    else {
      await chunkMultiDemo(demo, nodes, md)
    }
  }
}
