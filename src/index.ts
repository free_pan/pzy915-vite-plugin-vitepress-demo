import type { PluginOption, ResolvedConfig } from 'vite'
import type { MarkdownRenderer } from 'vitepress'
import { createMarkdownRenderer } from 'vitepress'
import { normalizePath } from 'vite'
import type { UserOptions } from './typing'
import { Parser } from './parser'

const vitePluginVitepressDemo = (_opt?: UserOptions): PluginOption => {
  let config: ResolvedConfig
  const options: UserOptions = Object.assign({}, _opt)
  let md: MarkdownRenderer
  let parser: Parser
  const aliasName = options.aliasName ?? '/@/VITEPRESS_DEMO'
  const virtualModule = 'virtual:vitepress-demo'
  const virtualModuleId = `\0${virtualModule}`
  return {
    /**
     * // important: 这个name必须与package.json中的name一致. 原因如下:
     * 这个name必须与package.json中的name一致，否则本地开发无法运行
     */
    name: '@pzy915/vite-plugin-vitepress-demo',
    enforce: 'pre',
    config(config) {
      const path = normalizePath(options.base || config.root || process.cwd())
      return {
        resolve: {
          alias: {
            [aliasName]: path,
          },
        },
        ssr: {
          /**
           * // important: noExternal的值必须与package.json中的name名保持一致
           * noExternal的值必须与package.json中的name名保持一致，否则其他项目引入之后，在打包时会出现如下异常:
           * [ERR_UNKNOWN_FILE_EXTENSION]: Unknown file extension ".vue"
           */
          noExternal: ['@pzy915/vite-plugin-vitepress-demo'],
        },
      }
    },
    async buildStart() {
      parser.cacheStore.clear()
      await parser.buildCache()
    },
    async configResolved(_config) {
      config = _config
      md = await createMarkdownRenderer(config.root, options?.markdown ?? {}, config.base ?? '/')
      parser = new Parser(options, config, md)
    },
    async configureServer(server) {
      await parser.setupServer(server)
    },
    resolveId(id) {
      if (id === virtualModule)
        return virtualModuleId
    },
    async transform(code, id) {
      if (id.endsWith(`lang.${parser.blockName}`))
        return 'export default {}'
      return await parser.transform(code, id)
    },
    load(id) {
      if (id === virtualModuleId)
        return parser.load()
    },
  }
}

export {
  vitePluginVitepressDemo,
}

export const VitePluginVitepressDemo = vitePluginVitepressDemo

export default vitePluginVitepressDemo
